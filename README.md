# Scrappe a database of all 'small requests' to the german Bundestags

## The API for fetching small requests
All small requests of the federal german government
are made available under 
`pdok.bundestag.de/treffer.php`.

And as far as I can see, these arguments may
be provided:
```
h := The number of results to throw away in the front.
q := The legislative periode concerned (currently 19)
dart := "Drucksache" , just keep it like this.
gtype := The general type of document. "Fragen/Anfragen" for this.
typ := "Kleine Anfrage"
```
This gives you an HTML Document consisting of a Table of 
Links to PDF Documents.
