"""
Fetch the small requests
from the Endpoint.
"""
import requests
import re

pattern = re.compile("<a href=\"(https:\/\/dserver.bundestag.de\/btd\/[0-9]+\/[0-9]+\/\d+.pdf)\"")

def fetch(legislative_periode = 19, i = 500):
    """Fetch and parse all the small requests of
    a legislative periodes.
    """
    results = []
    session = requests.Session()


    host = "https://pdok.bundestag.de/treffer.php"
    h = 0
    dart = "Drucksache"
    gtype = "Fragen/Anfragen"
    typ = "Kleine Anfrage"

    html = "pdf"
    while "pdf" in html:
        html = session.get(f"{host}", params =  { "h" : h
                                                , "dart": dart
                                                , "gtype" : gtype
                                                , "typ": typ
                                                , "q": f"{legislative_periode}/*"
                                                }).text

        for match in pattern.finditer(html):
            results.append(match.groups(1)[0])
        h += 100
        if h / 100 >= i:
            return results

    return results

results = fetch(19)
print(len(results))
print(results)
